import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full' },
    { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
    { path: 'home', loadChildren: './pages/home/home.module#HomePageModule' },
    { path: 'list', loadChildren: './pages/list/list.module#ListPageModule' },
    { path: 'plants', loadChildren: './pages/plants/plants.module#PlantsPageModule' },
    { path: 'add-plant', loadChildren: './pages/add-plant/add-plant.module#AddPlantPageModule' },
    { path: 'plant-list', loadChildren: './pages/plant-list/plant-list.module#PlantListPageModule' },
    { path: 'plant-details', loadChildren: './pages/plant-details/plant-details.module#PlantDetailsPageModule' },
    { path: 'account-settings', loadChildren: './pages/account-settings/account-settings.module#AccountSettingsPageModule' },
    { path: 'privacy-statement', loadChildren: './pages/privacy-statement/privacy-statement.module#PrivacyStatementPageModule' },
    { path: 'terms-of-service', loadChildren: './pages/terms-of-service/terms-of-service.module#TermsOfServicePageModule' },
    { path: 'plant-details2', loadChildren: './pages/plant-details2/plant-details2.module#PlantDetails2PageModule' },
    { path: 'add-to-compare', loadChildren: './pages/add-to-compare/add-to-compare.module#AddToComparePageModule' },
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
