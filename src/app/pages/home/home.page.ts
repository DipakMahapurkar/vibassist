import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationExtras } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { UtilityService } from '../../services/utility.service';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
    animations: [
        trigger('expand', [
            state('true', style({ height: '50px' })),
            state('false', style({ height: '0' })),
            transition('void => *', animate('0s')),
            transition('* <=> *', animate('250ms ease-in-out'))
        ])
    ]
})
export class HomePage implements OnInit {
    public plantList: Array<any> = [];
    public mainSegment: any = "alarms";
    private selectedPlantItem: any;
    public plantName: any = {};

    // plant list data
    public segmentModel: string = "myChannel";
    public dummyPlantDataList: Array<any> = [];
    public dummyExpandableListJsonObject: any;
    public channelRadioGroupValue: any;

    // Add plant
    public isViewAddPlantSection: boolean = false;
    public isViewPlantListSection: boolean = true;
    public addToMyChannelList: Array<any> = [];

    constructor(
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private utilityService: UtilityService
    ) {
        this.activatedRoute.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.plantName = this.router.getCurrentNavigation().extras.state.plantName;
            }
        });

        this.initPlantList();
        this.initAllChannelsExpandableList();
    }

    ngOnInit(): void {
        for (let index = 0; index < 10; index++) {
            this.dummyPlantDataList.push(
                {
                    id: index + 1,
                    channelName: "Turbine_Front_Brg_x",
                    channelValue: "72.48",
                    channelUnit: "mm/s RMS",
                    active: false
                }
            );
        }
    }

    private initAllChannelsExpandableList(): void {
        this.dummyExpandableListJsonObject = [{
            "id": 1,
            "name": "Group 1",
            "active": false,
            "machineArray": [{
                "id": 1,
                "name": "Machine 1",
                "active": false,
                "channelArray": [{
                    "id": 1,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                },
                {
                    "id": 2,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                },
                {
                    "id": 3,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                }]
            }, {
                "id": 2,
                "name": "Machine 2",
                "active": false,
                "channelArray": [{
                    "id": 1,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                },
                {
                    "id": 2,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                },
                {
                    "id": 3,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                }]
            }]
        },
        {
            "id": 2,
            "name": "Group 2",
            "active": false,
            "machineArray": [{
                "id": 1,
                "name": "Machine 1",
                "active": false,
                "channelArray": [{
                    "id": 1,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                },
                {
                    "id": 2,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                },
                {
                    "id": 3,
                    "active": false,
                    "channelName": "Turbine_Front_Brg_x",
                    "channelValue": "72.48",
                    "channelUnit": "mm/s RMS"
                }]
            }]
        }]
    };

    /**
     * initPlantList()
     */
    public initPlantList(): void {
        this.plantList.push(
            { id: 1, platName: "TSI_U1_ABC", badgeCount: 15 },
            { id: 2, platName: "TSI_U2_ABC", badgeCount: 2 },
            { id: 3, platName: "TSI_U3_ABC", badgeCount: 3 },
            { id: 4, platName: "TSI_U4_ABC", badgeCount: 4 },
            { id: 5, platName: "TSI_U5_ABC", badgeCount: 5 },
        );
    };

    /**
     * onClickOpenDetails(plant: any)
     */
    public onClickOpenDetails(plant: any) {
        console.log("Selected plant", plant);
        let navigationExtras: NavigationExtras = {
            state: {
                plantDetails: plant
            }
        };
        this.router.navigate(["plant-details2"], navigationExtras);
    };


    /**
     * onClickPlantItem(selectedPlant: any)
     */
    public onClickPlantItem(selectedPlant: any) {
        console.log("Selected plant ", selectedPlant);
        for (let i = 0; i < this.dummyPlantDataList.length; i++) {
            // Inactive all other items
            if (this.dummyPlantDataList[i].id != selectedPlant.id) this.dummyPlantDataList[i].active = false;
        }
        // active only clickable item
        selectedPlant.active = !selectedPlant.active;

        if (selectedPlant.active) {
            this.selectedPlantItem = selectedPlant;
        } else {
            this.selectedPlantItem = null;
        }
    };

    /**
     * goToPlantChartPage()
     */
    public goToPlantChartPage(graphNumber: number): void {
        if (this.selectedPlantItem) {
            this.selectedPlantItem["graphOption"] = graphNumber;
            let navigationExtras: NavigationExtras = {
                state: {
                    plantDetails: this.selectedPlantItem
                }
            };
            this.router.navigate(["plant-details"], navigationExtras);
        } else {
            this.utilityService.showToast("Please select the one of plat from plants");
        }
    };


    /**
     * removeItemFromPlantList()
     */
    public removeItemFromPlantList(): void {
        if (this.selectedPlantItem) {
            const index = this.dummyPlantDataList.findIndex(x => x.id === this.selectedPlantItem.id);
            this.dummyPlantDataList.splice(index, 1);
            this.utilityService.showToast(this.selectedPlantItem.channelName + " removed successfully");
            this.selectedPlantItem = null;

        } else {
            this.utilityService.showToast("Please select the one of plat from plants");
        }
    };

    /**
     * onClickAddNew()
     */
    public onClickAddNew(): void {
        this.isViewAddPlantSection = true;
        this.isViewPlantListSection = false;
        this.initAllChannelsExpandableList();
        this.addToMyChannelList = [];
    };

    /**
     * onPlantItemSelectedToAdd(selectedPlantToAdd: any)
     */
    public onPlantItemSelectedToAdd(selectedPlantToAdd: any): void {
        console.log("selected plant to add", selectedPlantToAdd);
        if (selectedPlantToAdd.active) {
            this.addToMyChannelList.push(selectedPlantToAdd);
        } else {
            if (this.addToMyChannelList.length != 0) {
                const index = this.addToMyChannelList.findIndex(x => x.id === selectedPlantToAdd.id);
                this.addToMyChannelList.splice(index, 1)
            }
        }
    };

    /**
     * onAddToMyChannels()
     */
    public onAddToMyChannels(): void {
        if (this.addToMyChannelList.length != 0) {
            for (let index = 0; index < this.addToMyChannelList.length; index++) {
                this.addToMyChannelList[index]["active"] = false;
                this.dummyPlantDataList.push(this.addToMyChannelList[index]);
            }
            this.isViewAddPlantSection = false;
            this.isViewPlantListSection = true;
        } else {
            this.utilityService.showToast("Please select at least one channel !!");
        }
    };

    /**
     * onCancelChannels()
     */
    public onCancelChannels() {
        this.isViewAddPlantSection = false;
        this.isViewPlantListSection = true;
    };

    /**
     * mainSegmentChange(mainSegment: any)
     */
    public mainSegmentChange(mainSegment: any) {
        console.log("Main segment selected", mainSegment);
    };

    /**
     * segmentChanged(selectedSegmentOption: any)
     */
    public segmentChanged(selectedSegmentOption: any): void {
        console.log("On segment change ", selectedSegmentOption);
        this.segmentModel = selectedSegmentOption;
        if (this.segmentModel == "allChannel") {
            this.initAllChannelsExpandableList();
            this.selectedPlantItem = null;
        } else if (this.segmentModel == "myChannel") {
            this.isViewAddPlantSection = false;
            this.isViewPlantListSection = true;
        }
    };

    public toggleGroup(item: any): void {
        for (let i = 0; i < this.dummyExpandableListJsonObject.length; i++) {
            //close all other active groups
            if (this.dummyExpandableListJsonObject[i].id != item.id) this.dummyExpandableListJsonObject[i].active = false;
        }
        //expand only the clicked group
        item.active = !item.active;
    };

    public toggleGroupMachine(machine: any): void {
        for (let index = 0; index < this.dummyExpandableListJsonObject.length; index++) {
            for (let indexJ = 0; indexJ < this.dummyExpandableListJsonObject[index].machineArray.length; indexJ++) {
                if (this.dummyExpandableListJsonObject[index].machineArray[indexJ].id != machine.id) {
                    this.dummyExpandableListJsonObject[index].machineArray[indexJ].active = false;
                }
            }
        }
        machine.active = !machine.active;
    };

    /* public radioSelect(plant: any): void {
        this.selectedPlantItem = plant;
    }; */

    /**
     * onChannelSelected(channel: any)
     */
    public onChannelSelected(channel: any) {
        if (channel.active) {
            this.selectedPlantItem = channel;
        } else {
            this.selectedPlantItem = null;
        }
    };

    /* public radioGroupChange(event: any): void {
        console.log("radioGroupChange", event.detail.value);
        if (event.detail.value === undefined) {
            this.selectedPlantItem = null;
        }
    }; */
}