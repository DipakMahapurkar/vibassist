import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { UtilityService } from '../../services/utility.service';

@Component({
    selector: 'app-plants',
    templateUrl: './plants.page.html',
    styleUrls: ['./plants.page.scss'],
})
export class PlantsPage implements OnInit {

    public plants: Array<any> = [];

    constructor(
        private router: Router,
        private utilityService: UtilityService
    ) { }

    ngOnInit() {
        this.initPlants();
    }

    private initPlants(): void {
        this.plants.push(
            { id: 1, plantName: "Plant 1" },
            { id: 2, plantName: "Plant 2" },
            { id: 3, plantName: "Plant 3" },
        );
    };

    /**
     * onClickPlant(plantName: any)
     */
    public onClickPlant(plantName: any): void {
        console.log("Selected plant", plantName);
        this.utilityService.selectedPlantName = plantName;
        let navigationExtras: NavigationExtras = {
            state: {
                plantName: plantName
            }
        };
        this.router.navigate(["home"], navigationExtras);
    };
}
