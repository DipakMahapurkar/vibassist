import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { UtilityService } from '../../services/utility.service';

@Component({
    selector: 'app-plant-details2',
    templateUrl: './plant-details2.page.html',
    styleUrls: ['./plant-details2.page.scss'],
    animations: [
        trigger('expand', [
            state('true', style({ height: '50px' })),
            state('false', style({ height: '0' })),
            transition('void => *', animate('0s')),
            transition('* <=> *', animate('250ms ease-in-out'))
        ])
    ]
})
export class PlantDetails2Page implements OnInit {
    public dummyList: Array<any> = [];
    public plantDetailsData: any = {};
    public plantName: any;


    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private utilityService: UtilityService
    ) {
        this.plantName = this.utilityService.selectedPlantName;
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.plantDetailsData = this.router.getCurrentNavigation().extras.state.plantDetails;
            }
        });
    }

    public toggleGroup(item: any): void {
        for (let i = 0; i < this.dummyList.length; i++) {
            //close all other active groups
            if (this.dummyList[i].machineId != item.machineId) this.dummyList[i].active = false;
        }
        //expand only the clicked group
        item.active = !item.active;
    };

    ngOnInit() {
        this.dummyList = [
            {
                "machineId": 1,
                "machineTitle": "Machine 1",
                "alarmsCount": 5,
                "active": false,
                "alarms": [
                    { "alarmId": 1, "alarmTitle": "Channel 1", "isActive": true },
                    { "alarmId": 2, "alarmTitle": "Channel 2", "isActive": true },
                    { "alarmId": 3, "alarmTitle": "Channel 3", "isActive": false },
                    { "alarmId": 4, "alarmTitle": "Channel 4", "isActive": true },
                    { "alarmId": 5, "alarmTitle": "Channel 5", "isActive": false }
                ]
            },
            {
                "machineId": 2,
                "machineTitle": "Machine 2",
                "alarmsCount": 3,
                "active": false,
                "alarms": [
                    { "alarmId": 1, "alarmTitle": "Channel 1", "isActive": true },
                    { "alarmId": 2, "alarmTitle": "Channel 2", "isActive": true },
                    { "alarmId": 3, "alarmTitle": "Channel 3", "isActive": false }
                ]
            }
        ];
    }
}