import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlantDetails2Page } from './plant-details2.page';


describe('PlantDetails2Page', () => {
  let component: PlantDetails2Page;
  let fixture: ComponentFixture<PlantDetails2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlantDetails2Page],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlantDetails2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
