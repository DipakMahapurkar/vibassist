import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PlantDetails2Page } from './plant-details2.page';

const routes: Routes = [
  {
    path: '',
    component: PlantDetails2Page
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
  ],
  declarations: [PlantDetails2Page]
})
export class PlantDetails2PageModule { }
