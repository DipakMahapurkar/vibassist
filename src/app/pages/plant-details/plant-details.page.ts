import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as HighCharts from 'highcharts';
import { UtilityService } from '../../services/utility.service';


@Component({
    selector: 'app-plant-details',
    templateUrl: './plant-details.page.html',
    styleUrls: ['./plant-details.page.scss'],
})
export class PlantDetailsPage implements OnInit {
    public plantDetailsData: any = {};
    public plantName: any;
    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private utilityService: UtilityService

    ) {
        this.plantName = this.utilityService.selectedPlantName;
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.plantDetailsData = this.router.getCurrentNavigation().extras.state.plantDetails;
            }
        });
    }

    ngOnInit() {
        this.plotSimpleBarChart();
    }

    plotSimpleBarChart() {
        let myChart = HighCharts.chart('highcharts', {
            chart: {
                type: 'column',
                renderTo: document.getElementById('highcharts'),
                margin: 0,
                width: 400,
                height: 300,
            },

            yAxis: [{
                className: 'highcharts-color-0',
                title: {
                    text: 'Primary axis'
                }
            }, {
                className: 'highcharts-color-1',
                opposite: true,
                title: {
                    text: 'Secondary axis'
                }
            }],

            plotOptions: {
                column: {
                    borderRadius: 5
                }
            },

            series: [
                {
                    name: 'Jane',
                    type: undefined,
                    data: [1, 3, 2, 4]
                },
                {
                    name: 'John',
                    type: undefined,
                    data: [324, 124, 547, 221],
                }
            ]
        });
    }
}
