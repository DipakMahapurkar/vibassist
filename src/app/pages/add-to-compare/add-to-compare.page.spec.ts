import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddToComparePage } from './add-to-compare.page';

describe('AddToComparePage', () => {
  let component: AddToComparePage;
  let fixture: ComponentFixture<AddToComparePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddToComparePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddToComparePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
