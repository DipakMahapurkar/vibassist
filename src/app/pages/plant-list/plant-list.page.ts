import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-plant-list',
    templateUrl: './plant-list.page.html',
    styleUrls: ['./plant-list.page.scss'],
})
export class PlantListPage implements OnInit {
    public segmentModel: string = "myChannel";
    public dummyPlantDataList: Array<any> = [];
    public dummyExpandableListJsonObject: any = {};
    public showLevel1: boolean = true;
    public showLevel2: boolean = false;
    public showLevel3: boolean = false;
    public addToMyChannelFlag: boolean = false;
    public viewAllChannelFlag: boolean = true;

    constructor() { }

    ngOnInit() {
        for (let index = 0; index < 10; index++) {
            this.dummyPlantDataList.push(
                {
                    dummyTitle: "Turbine_Front_Brg_x",
                    dummyValue: "72.48",
                    dummyUnit: "mm/s RMS",
                }
            );
        }

        this.dummyExpandableListJsonObject =
            {
                "mainTitle": "Group",
                "groupDataItems": [
                    { "id": 1, "name": "Group 1" },
                    { "id": 2, "name": "Group 2" },
                    { "id": 3, "name": "Group 3" },
                    { "id": 4, "name": "Group 4" },
                    { "id": 5, "name": "Group 5" },
                    {
                        "subTitle": "Machines",
                        "machineDataItems": [
                            { "id": 1, "name": "Machine 1" },
                            { "id": 2, "name": "Machine 2" },
                            { "id": 3, "name": "Machine 3" },
                            { "id": 4, "name": "Machine 4" },
                            { "id": 5, "name": "Machine 5" },
                            {
                                "subSubTitle": "Channels",
                                "channelDataItems": [
                                    { "id": 1, "name": "Channel 1" },
                                    { "id": 2, "name": "Channel 2" },
                                    { "id": 3, "name": "Channel 3" },
                                    { "id": 4, "name": "Channel 4" },
                                    { "id": 5, "name": "Channel 5" },
                                ],
                            }
                        ],
                    }
                ],
            }
    }

    /**
     * segmentChanged(selectedSegmentOption: any)
     */
    public segmentChanged(selectedSegmentOption: any, optionalFlag?: boolean): void {
        console.log("On segment change ", selectedSegmentOption);
        this.segmentModel = selectedSegmentOption;
        if (optionalFlag) {
            this.addToMyChannelFlag = true;
            this.viewAllChannelFlag = false;
        } else {
            this.addToMyChannelFlag = false;
            this.viewAllChannelFlag = true;
        }
    };

    /**
     * toggleLevel(level: any)
     */
    public onToggleLevel(level: any): void {
        switch (level) {
            case 1:
                this.showLevel1 = !this.showLevel1;
                this.showLevel2 = !this.showLevel2;
                this.showLevel3 = !this.showLevel3
                break;
            case 2:
                this.showLevel2 = !this.showLevel2;
                this.showLevel3 = !this.showLevel3
                break

            case 3:
                this.showLevel3 = !this.showLevel3
                break;
        }
    };

    /**
     * onClickAddNew()
     */
    public onClickAddNew(): void {
        this.segmentChanged("allChannel", true);
    };
}