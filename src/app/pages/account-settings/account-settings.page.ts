import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-account-settings',
    templateUrl: './account-settings.page.html',
    styleUrls: ['./account-settings.page.scss'],
})
export class AccountSettingsPage implements OnInit {

    public dummySettingList: Array<any> = [];

    constructor() { }

    ngOnInit() {
        this.dummySettingList.push(
            { id: 1, settingLabel: "About Plant", routingUrl: "/account-settings" },
            { id: 2, settingLabel: "Change Plant", routingUrl: "/account-settings" },
            { id: 3, settingLabel: "Add Plant", routingUrl: "/add-plant" },
            { id: 4, settingLabel: "Change Password", routingUrl: "/account-settings" },
            { id: 5, settingLabel: "Terms of Service", routingUrl: "/terms-of-service" },
            { id: 6, settingLabel: "Privacy Statement", routingUrl: "/privacy-statement" }
        );
    }
}