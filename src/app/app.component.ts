import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html'
})
export class AppComponent {
    public appPages = [
        {
            title: 'Home',
            url: '/home',
            icon: 'home'
        },
        {
            title: 'Account Setting',
            url: '/account-settings',
            icon: 'home'
        },
        {
            title: 'Push Notifications',
            url: '/home',
            icon: 'list'
        },

        {
            title: 'Privacy Statement',
            url: '/privacy-statement',
            icon: 'list'
        },
        {
            title: 'Terms of Service',
            url: '/terms-of-service',
            icon: 'list'
        }
    ];

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private storage: Storage,
        private router: Router,
        private menuCtrl: MenuController
    ) {
        this.initializeApp();
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.singleTimeLogin();
        });
    }

    /**
     * singleTimeLogin()
     */
    public singleTimeLogin(): void {
        this.storage.get("IS_LOGIN").then((loggedInUserData: any) => {
            if (loggedInUserData) {
                this.router.navigateByUrl("plants");
            } else {
                this.router.navigateByUrl("login");
            }
        });
    };

    /**
     * onLogout()
     */
    public onLogout() {
        this.storage.clear().then(() => {
            if (this.menuCtrl.isOpen()) {
                this.menuCtrl.close();
            }
            this.router.navigateByUrl("login");
        });
    };
}
