import { Injectable } from '@angular/core';
import { LoadingController, ToastController } from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class UtilityService {
    public loader: any = null;
    public selectedPlantName: any;

    constructor(
        private loadingCtrl: LoadingController,
        private toastCtrl: ToastController,
    ) { }

    /* Common function for showing the loading 
          @functionName: showLoader(),
          @parameters: message: string  
      */
    private async showLoadingHandler(message: string) {
        if (this.loader == null) {
            this.loader = await this.loadingCtrl.create({
                message: message
            });
            this.loader.present();
        } else {
            this.loader.data.content = message;
        }
    };

    /* Common function for hide the loading 
        @functionName: hideLoader()
        @parameters: none
    */
    private hideLoadingHandler() {
        if (this.loader != null) {
            this.loader.dismiss();
            this.loader = null;
        }
    };

    /* Common function for showing the loading 
        @functionName: showLoader(),
        @parameters: content: string  
    */
    public showLoader(message: string) {
        this.showLoadingHandler(message);
    };

    /* Common function for hide the loading 
        @functionName: hideLoader()
        @parameters: none
    */
    public hideLoader() {
        this.hideLoadingHandler();
    };

    /* Common function for showing the toast
        @functionName: showToast(), 
        @parameters: message: string, duration?: number        
    */
    public async showToast(message: string, duration?: number) {
        let toast = await this.toastCtrl.create({
            message: message,
            duration: duration ? duration : 3000,
        });
        toast.present();
    };
}
